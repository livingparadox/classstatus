@import 'bower_components/bootstrap-sass-official/assets/stylesheets/_bootstrap.scss';
@import url(http://fonts.googleapis.com/css?family=Glegoo|Open+Sans:300,400,600,700);
@import url('//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css');
@import 'bootstrap-extensions.scss';

/* Universal */

body > .ember-view {
	display: flex;
	min-height: 100vh;
	flex-direction: column;
}

#main {
	flex: 1;
	display: flex;
}

#main > .cover {
	margin-top: -20px;
	margin-bottom: -20px;
	width: 100%;
}

.navbar-default {
	border-color: #CFCFCF;
}

/* Header */

#header {

	.navbar {
		min-height: 60px;
		border-right: none;
		border-left: none;
		border-radius: 0;
	}

	.navbar-brand	{
		padding:0px 15px;
		height:60px;
		line-height:60px;
	}

	.navbar-brand span {
		font-family: 'Glegoo', serif;
		color: black;
		vertical-align:middle;
	}

	.navbar-nav > li > a {
		font-family: 'Open Sans', sans-serif;
		padding-top:0px;
		padding-bottom:0px;
		line-height:60px;
	}

	.navbar-form {
		margin-top:0px;
		margin-bottom:0px;
		padding-top:0px;
		padding-bottom:0px;
		line-height:60px;
		margin-right:-10px;
	}

	.form-inline .form-control, .form-inline .navbar-form {
		padding-left: 2.75em;
		height: 38px;
		color: #797979;
		padding-left: 35px;
		font-weight: 600;
		padding-right: 76px;
		font-family:'Open Sans', sans-serif;
		-webkit-box-shadow: 0px 0px 0px 0px rgba(0,0,0,0);
		-moz-box-shadow: 0px 0px 0px 0px rgba(0,0,0,0);
		box-shadow: 0px 0px 0px 0px rgba(0,0,0,0);
	}

	input:focus {
		outline:0 !important;
	}

	.search-icon {
		position: absolute;
		left: 0.8em;
		color: #D6D6D6;

		.fa {
			margin-top: 1.2em;
			font-size: 1.3em;
		}
	}
	.searchbutton {
		border-width: 1px;
		border-color: #c6d1d6 #c6c6c6 #b2bbbe;
		border-style: solid;
		border-radius: 3px;
		font-family:'Open Sans', sans-serif;
		right: 1em;
		display: inline-block;
		vertical-align: top;
		height: 25px;
		position: absolute;
		top: calc(50% - 11.5px);
		font-size: 11px;
		color: #797979;
		padding: 0px 8px;
		line-height: 23px;
		font-weight: 600;
		text-align: center;
		-o-transition: .2s;
		-ms-transition: .2s;
		-moz-transition: .2s;
		-webkit-transition: .2s;
		transition: .2s;
		background-color: #e0e6e7;
		background: rgba(255,255,255,0.5);
		background: -webkit-linear-gradient(270deg, rgba(255,255,255,0.5) 0%, rgba(0,0,0,0.05) 100%) #e0e6e7;
		background: -moz-linear-gradient(270deg, rgba(255,255,255,0.5) 0%, rgba(0,0,0,0.05) 100%) #e0e6e7;
		background: -o-linear-gradient(270deg, rgba(255,255,255,0.5) 0%, rgba(0,0,0,0.05) 100%) #e0e6e7;
		background: -ms-linear-gradient(270deg, rgba(255,255,255,0.5) 0%, rgba(0,0,0,0.05) 100%) #e0e6e7;
		background: linear-gradient(180deg, rgba(255,255,255,0.5) 0%, rgba(0,0,0,0.05) 100%) #e0e6e7;
	}

	.searchbutton:hover {
		background-color:#DADEDE
	}

	.searchbutton:focus	{
		outline:0;
	}

	.searchbutton:active {
		-webkit-box-shadow:inset 0 2px 6px rgba(0,0,0,0.24);
		-moz-box-shadow:inset 0 2px 6px rgba(0,0,0,0.24);
		box-shadow:inset 0 2px 6px rgba(0,0,0,0.24);
	}
}

/* Footer */

#footer {
	.navbar {
		min-height: 80px;
	}

	ul a {
		text-decoration: none;
		color:#AAA;
	}

	ul.social a {
		color:#aaaaaa;
		vertical-align: middle;
		margin-right: 7px;
		-o-transition:.5s;
		-ms-transition:.5s;
		-moz-transition:.5s;
		-webkit-transition:.5s;
		transition:.5s;
		padding: 0px 5px;
		line-height:80px;
	}


	ul.social i {
		vertical-align: middle;
		font-size: 32px;
	}

	ul.social a:hover {
		color:#858585 !important;
	}

	ul.social a:last-child {
		margin-right: 0px;
	}

	.social p.navbar-text {
		color:#aaaaaa;
		font-size: 14px;
		margin-right: 3px;
		font-family: 'Open Sans', sans-serif;
		vertical-align: middle;
		margin-top:0px;
		margin-bottom:0px;
		line-height:80px;
	}

	.navbar-header .navbar-text {
		margin-right: 8px;
		margin-left: 0px;
	}

	.navbar-header .navbar-text, .navbar-left li	{
		color: #aaa;
		font-family: 'Open Sans', sans-serif;
		line-height: 80px;
		padding-top:0px;
		padding-bottom:0px;
		margin-top:0px;
		margin-bottom:0px;
	}
	.navbar-left li::before {
		content: '·';
		display: inline-block;
		line-height:80px;
	}

	.navbar-left > li > a {
		padding:0px 7px;
		line-height:80px;
		display: inline-block;
	}
}

/* Sign In */

#signin {
	background:url('/assets/classroom.png');
	background-size:cover;


	.widget {
		width: 316px;
		height: 345px;
		margin:95px auto 0;
		border-radius: 5px;
		background-color: #ffffff;
		box-shadow: 0 1px 5px rgba(0, 0, 0, 0.25);


		h1 {
			font-size: 24px;
			font-weight: 300;
			color: #4f4f4f;
			text-align: center;
			padding-top: 21px;
			font-family: 'Open Sans', sans-serif;
			margin-left: 13px;
		}

		textarea, input {
			outline: none;
		}

		.form	{
			width: 258px;
			height: 37px;
			display: block;
			padding: 0 10px 0 12px;
			border: 1px solid #dddddd;
			border-radius: 3px;
			background-color: #f0f0f0;
			font-size: 0.929em;
			font-weight: 400;
			line-height: 1.48;
			margin-bottom: 14px;
			color: rgb(133, 133, 133);
			font-size: 13px;
			font-family: 'Open Sans', sans-serif;
		}

		#checked	{
			font-size: 11px;
			color: rgb(77, 75, 75);
			font-family: 'Open Sans', sans-serif;
			height: 14px;
			vertical-align:top;
			line-height:14px;
		}

		#box {
			margin-right: 5px;
			margin-left: 0px;
			height: 14px;
			width: 14px;
			margin-top: 0px;
			vertical-align:top;
		}

		#forgot {
			color: #3190d3;
			font-family: 'Open Sans', sans-serif;
			text-decoration: none;
			font-size: 11px;
			float: right;
			vertical-align:middle;
			line-height:14px;
		}

		.normal	{
			width: 257px;
			margin-left: 28.5px;
			vertical-align:top;
			display: inline-block;
			margin-right: 28.5px;
			height: 164px;
		}

		button:active, button:hover, button:focus {
			outline: none;
		}

		.line {
			width: 1px;
			height: 202px;
			margin-right: -5px;
			margin-left: -5px;
			display: inline-block;
			background-color: #eeeeee;
		}

		.login	{
			height: 39px;
			border-color: #6C94CF rgb(88, 121, 170) rgb(52, 91, 149);
			background-color: rgb(62, 111, 182);
			box-shadow: 0 1px 2px rgba(71, 123, 200, 0.53), 0 0 2px rgb(69, 112, 175) inset;
			text-shadow: 0 1px 1px rgba(0, 0, 0, 0.36);
			width: 258px;
			font-family: 'Open Sans', sans-serif;
			border-width: 1px;
			border-style: solid;
			font-weight: 600;
			border-radius: 3px;
			background-image: linear-gradient(180deg, rgba(255, 255, 255, 0.2) 0%, rgba(89, 89, 89, 0.21) 100%);
			font-size: 14px;
			margin-top:8px;
			line-height: 1.38;
			text-align: center;
			color: rgb(255, 255, 255);
			resize: none;
			cursor: pointer;
			padding-right: 10px;
		}

		.login:active	{
			background-color: rgb(48, 85, 139);
		}

		.options	{
			height: 15px;
			margin-top: -6px;

		}

		.form-group	{
			margin-bottom: -5px;
		}

		.forms {
			margin-top: 24px;
			height: 218px;
		}

		.footer	{
			width: 100%;
			height: 57px;
			border-top: 1px solid #e5e5e5;
			border-bottom-right-radius: 5px;
			border-bottom-left-radius: 5px;
			background-color: #eaeaea;
		}

		.footer p {
			font-family: 'Open Sans', sans-serif;
			display: inline-block;
			font-size: 13px;
			margin: 0px;
			color: rgb(79, 79, 79);
		}

		.footer a {
			font-family: 'Open Sans', sans-serif;
			display: inline-block;
			font-size: 13px;
			margin: 0px;
			text-decoration: none;
			color: rgb(49, 144, 211);
		}

		.content	{
			text-align: center;
			line-height:57px;
		}

		.socialmedia	{
			float: right;
			width:313px;
			height: 202px;
			display: inline-block;
			margin-top: 1px;
		}

		.facebook {
			width: 258px;
			height: 48px;
			margin-left: 27px;
			padding-left: 4px;
			font-family: 'Open Sans', san-serif;
			border-color: rgb(189, 189, 189) rgb(88, 121, 170) rgb(52, 91, 149);
			background-color: #3b5998;
			box-shadow: 0 1px 2px rgba(71, 123, 200, 0.53), 0 0 2px rgb(69, 112, 175) inset;
			text-shadow: 0 1px 1px rgba(0, 0, 0, 0.36);
			display: block;
			width: 258px;
			border-width: 1px;
			border-style: solid;
			border-radius: 3px;
			background-image: linear-gradient(180deg, rgba(255, 255, 255, 0.2) 0%, rgba(89, 89, 89, 0.21) 100%);
			font-size: 14px;
			line-height: 1.38;
			text-align: center;
			color: rgb(255, 255, 255);
			cursor: pointer;
		}

		.facebook:active	{
			background-color: #31497a
		}

		.icon	{
			font-size: 30px;
			line-height: 0px;
			vertical-align: middle;
			padding-right: 6px;
		}

		.twitter {
			display: block;
			height: 48px;
			width: 258px;
			margin: 0 auto;
			padding-left: 4px;
			font-family: 'Open Sans', san-serif;
			border-color: rgba(132, 192, 235, 0.68) rgb(95, 156, 201) rgb(98, 139, 170);
			background-color: #4595d2;
			box-shadow: 0 1px 2px rgba(105, 165, 210, 0.68), 0 0 2px rgb(80, 135, 177) inset;
			text-shadow: 0 1px 1px rgba(0, 0, 0, 0.27);
			border-width: 1px;
			border-style: solid;
			border-radius: 3px;
			background-image: linear-gradient(180deg, rgba(255, 255, 255, 0.2) 0%, rgba(89, 89, 89, 0.21) 100%);
			font-size: 14px;
			line-height: 1.38;
			margin-left: 27px;
			margin-top: 12px;
			text-align: center;
			cursor: pointer;
			color: rgb(255, 255, 255);
		}

		.twitter:active	{
			background-color: rgb(64, 123, 167);
		}

		#twitter {
			margin-left: -17px;
		}

		#close {
			position: relative;
			float: right;
			color: #A0A2A4;
			display: none;
			margin-right: 13px;
			margin-top: 11px;
			font-size: 21px;
		}
	}
}

/* Registration */

#registration {
	background:url('/assets/classroom_alt.png');
	background-size:cover;
}

.registration {

	.widget {
		margin: 32px auto 0;
		width: 445px;
		-webkit-border-radius: 5px;
		-moz-border-radius: 5px;
		border-radius: 5px;
		background-color: #ffffff;
		box-shadow: 0 2px 5px rgba(0, 0, 0, 0.35);
		/* Possible new shadow
		-webkit-box-shadow: 0 2px 26px rgba(0, 0, 0, .3), 0 0 0 1px rgba(0, 0, 0, .1);
		*/
	}

	div.header {
		width: 445px;
		height: 75px;
		margin-left: 0px;
		background-color: rgba(222, 222, 222, 0);
	}

	div.header h2 {
		color: rgb(128, 128, 128);
		font-family: 'Open Sans', sans-serif;
		font-size: 19px;
		font-weight: 300;
		line-height: 75px;
		margin-left: 32px;
		margin-top:0px !important;
		margin-bottom:0px !important;
	}

	div.forms {
		background-color: #f4f4f4;
		-webkit-border-radius: 5px;
		-moz-border-radius: 5px;
		border-radius: 5px;
	}

	div.mobile {
		display: none;
	}

	div.completed {
		display: none;
	}

	div.row {
		display: table;
		height: 69.5px;
		margin: 0 auto;
		border-top: 1px solid #e2e2e2;
		background-color: #F8F8F8;
	}

	div.form {
		text-align: center;
		display: table-cell;
		vertical-align: middle;
		width: 445px;
	}

	div.row:nth-child(2n+1) {
		background-color: #F4F4F4;
	}

	div.row:last-child {
		border-bottom: 1px solid #e2e2e2;
	}

	div.row label {
		font-family: 'Open Sans', sans-serif;
		font-size: 14px;
		display: inline-block;
		font-weight: 600;
		color:rgb(80, 80, 80);
		text-align: right !important;
		margin-right: 7px;
		width: 74px;
		vertical-align: middle;
		margin-bottom: 0px !important;
	}

	div.row input {
		width: 290px;
		height: 40px;
		display: inline-block;
		outline: none;
		border: 1px solid #e2e2e2;
		-webkit-border-radius: 3px;
		-moz-border-radius: 3px;
		border-radius: 3px;
		font-size: 13px;
		padding:0 10px;
		vertical-align: middle;
		font-family: 'Open Sans', sans-serif;
	}
	div.row input:focus {
		border: 1px solid #aaaaaa;
	}
	div.row input.valid {
		border: 1px solid #2ecc71;
	}
	div.row input.invalid {
		border: 1px solid #e74c3c;
	}

	label#confirm {
		line-height:15.129607200622559px;
	}

	div.progress {
		display: none;
		height: 2px;
		width: 0%;
		background: #609cb6;
	}

	div.footer {
		height:72px;
		-webkit-border-bottom-right-radius: 5px;
		-webkit-border-bottom-left-radius: 5px;
		-moz-border-radius-bottomright: 5px;
		-moz-border-radius-bottomleft: 5px;
		border-bottom-right-radius: 5px;
		border-bottom-left-radius: 5px;
		display: table;
		background-color:white;
	}

	div.footer .info {
		display: table-cell;
		vertical-align: middle;
		width: 445px;
	}

	div.footer .info span a.step {
		color: rgb(153, 153, 153);
	}

	div.footer span {
		font-size:12px;
		color: rgb(153, 153, 153);
		font-family: 'Open Sans', sans-serif;
		float: left;
		display: inline-block;
		margin-left: 33px;
		line-height: 38px;
	}

	div.footer .buttons {
		float: right;
		display:inline-block;
		margin-right: 36px;
	}

	div.footer .buttons button.cancel {
		width: 91px;
		border-color: rgb(192, 198, 201) rgb(255, 255, 255) rgba(230, 230, 230, 1) rgb(188, 188, 188);
		background: rgba(208, 208, 208, 0.25);
		background: -webkit-linear-gradient(270deg, rgba(208, 208, 208, 0.25) 0%, rgba(15, 15, 15, 0.16) 100%) #FFFFFF;
		background: -moz-linear-gradient(270deg, rgba(208, 208, 208, 0.25) 0%, rgba(15, 15, 15, 0.16) 100%) #FFFFFF;
		background: -o-linear-gradient(270deg, rgba(208, 208, 208, 0.25) 0%, rgba(15, 15, 15, 0.16) 100%) #FFFFFF;
		background: -ms-linear-gradient(270deg, rgba(208, 208, 208, 0.25) 0%, rgba(15, 15, 15, 0.16) 100%) #FFFFFF;
		background: linear-gradient(180deg, rgba(208, 208, 208, 0.25) 0%, rgba(15, 15, 15, 0.16) 100%) #FFFFFF;
		color: rgb(68, 68, 68);
		height: 37px;
		border-width: 1px;
		margin: auto 0;
		border-radius: 3px;
		font-weight: 400;
		text-align: center;
		font-family: 'Open Sans', sans-serif;
		font-size: 13px;
		outline: none;
		cursor: pointer;
		margin-right: 5px;
	}

	div.footer .buttons button.cancel:active {
		background-color:#D2D2D2;
		box-shadow: none;
		border:none;
	}

	.next, .submit {
		width: 91px;
		border-color: rgb(192, 198, 201) rgb(255, 255, 255) rgba(230, 230, 230, 1) rgb(188, 188, 188);
		background: rgba(208, 208, 208, 0.25);
		background: -webkit-linear-gradient(270deg, rgba(208, 208, 208, 0.25) 0%, rgba(15, 15, 15, 0.16) 100%) #FFFFFF;
		background: -moz-linear-gradient(270deg, rgba(208, 208, 208, 0.25) 0%, rgba(15, 15, 15, 0.16) 100%) #FFFFFF;
		background: -o-linear-gradient(270deg, rgba(208, 208, 208, 0.25) 0%, rgba(15, 15, 15, 0.16) 100%) #FFFFFF;
		background: -ms-linear-gradient(270deg, rgba(208, 208, 208, 0.25) 0%, rgba(15, 15, 15, 0.16) 100%) #FFFFFF;
		background: linear-gradient(180deg, rgba(208, 208, 208, 0.25) 0%, rgba(15, 15, 15, 0.16) 100%) #FFFFFF;
		color: rgb(68, 68, 68);
		height: 37px;
		border-width: 1px;
		margin: auto 0;
		border-radius: 3px;
		font-weight: 400;
		text-align: center;
		font-family: 'Open Sans', sans-serif;
		font-size: 13px;
		outline: none;
		cursor: pointer;
	}

	button.next:active, button.submit:active {
		background-color: #D2D2D2;
		box-shadow: none;
		border: none;
	}

	.completed	{
		padding: 50px 0;
		display: table;
		border-bottom: 1px solid #e2e2e2;
		border-top: 1px solid #e2e2e2;
	}

	.completed h3	{
		font-family: 'Open Sans', sans-serif;
		font-size: 30px;
		font-weight: 500;
		color: #757575;
		margin:0;
		margin-top: -8px;
		text-align: center;
	}

	.completed p {
		font-size: 16px;
		color: #757575;
		width: 254px;
		line-height: 24px;
		text-align: center;
		font-family: 'Open Sans', sans-serif;
		margin: 0 96px;
	}

	.check {
		text-align: center;
		display: table-cell;
		vertical-align: middle;
	}

	.completed #checkmark	{
		font-size: 63px;
		margin:10px 0;

	}

	.completed .fa-circle {
		color: #4FC343;
	}

	.completed .fa-check	{
		color:white;
	}
}

/* 404 Errors */

#error {
	background-image:url('/assets/error_trans.png');
	background-size:cover;
	background-color:#FFFFFF;

  display: flex;
  align-items: center;
  justify-content: center;
}

.error	{

	.widget	{
		border-width: 1px;
		border-color: rgb(203, 203, 203) rgb(190, 190, 190) rgb(152, 152, 152);
		border-style: solid;
		border-radius: 5px;
		width:200px;
		background-color: rgb(255, 255, 255);
		box-shadow: 0 1px 4px rgba(0, 0, 0, 0.11);
		width: 350px !important;
		padding: 30px 0px;

    margin: auto;
	}

	h1 {
		font-family: 'Open Sans', sans-serif;
		font-size: 36px;
		font-weight:700;
		margin-top: 0px;
		margin-bottom: 8px;
		text-align:center;
	}

	p {
		font-family: 'Open sans' sans-serif;
		font-size: 19px;
		font-weight: 300;
		margin-top: -8px;
		text-align:center;
		margin-bottom:6px;
	}

}
